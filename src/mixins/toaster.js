import { mapState } from "vuex";

const toaster = {
  computed: {
    ...mapState({
      toast: (state) => state.toast,
    }),
  },
  watch: {
    toast: {
      handler() {
        console.log("changed detected in toaster");
        let toast = this.$store.state.toast;
        toast.type === "success"
          ? this.$toasted.success(toast.message)
          : this.$toasted.error(toast.message);
      },
      deep: true,
    },
  },
};

export default toaster;
