export default {
  allProducts: ({ allProducts }) => {
    console.log("---------allproducts---------------", allProducts);
    return allProducts.map(({ Id, Image_Name, P_Name, Lowest_Ask }) => {
      return {
        id: Id,
        img: Image_Name,
        name: P_Name,
        price: Lowest_Ask || "--",
      };
    });
  },
  productDetails: ({ productDetails }) => {
    console.log("GETTERS PRODUCT DETAILS", productDetails);
    return {
      title: productDetails.P_Name,
      lowestAsk: productDetails.Lowest_Ask || "--",
      highestBid: productDetails.Highest_Bid || "--",
      condition: productDetails.Description,
      brand: productDetails.Brand,
      retailPrice: productDetails.Retail_Price,
      verification: productDetails.Verification,
    };
  },
  productImages: ({ productImages }) =>
    productImages.map(({ Image_Name }) => Image_Name),
  filteredBids: ({ bids }) =>
    bids.map(({ User_Id, Bid }) => {
      return { id: User_Id, bid: Bid };
    }),
  filteredAsks: ({ asks }) =>
    asks.map(({ User_Id, Ask }) => {
      return { id: User_Id, ask: Ask };
    }),
};
