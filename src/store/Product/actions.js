import router from "./../../router/index";

export default {
  getProduct: ({ dispatch, state, rootState }) => {
    rootState.loading = true;
    const productContent = axios.get(
      `/mostPopularProduct/${router.currentRoute.params.id}`
    );
    const productImages = axios.get(
      `/productImages/getimagebyid/${router.currentRoute.params.id}`
    );
    return Promise.all([productContent, productImages])
      .then((res) => {
        rootState.loading = false;
        state.productDetails = res[0].data;
        state.productId = res[0].data.Id;
        state.productName = res[0].data.P_Name;
        state.productImages = res[1].data;
      })
      .catch(() => {
        rootState.loading = false;
      });
  },
  getAllProducts: ({ commit, rootState }) => {
    commit("getAllProducts", rootState);
  },
  getBids: ({ commit, rootState }) => {
    commit("getBids", rootState);
  },
  getAsks: ({ commit, rootState }) => {
    commit("getAsks", rootState);
  },
  // getting the product content
  getProductContent: ({ commit, dispatch }) => {
    dispatch("getProduct").then(() => {
      dispatch("getBids");
      dispatch("getAsks");
    });
  },
  placeBid: ({ commit, rootState }, bidAmount) => {
    if (bidAmount) {
      commit("placeBid", { bidAmount, rootState });
    }
  },
  placeAsk: ({ commit, rootState }, askAmount) => {
    if (askAmount) {
      commit("placeAsk", { askAmount, rootState });
    }
  },
  updateBid: ({ commit, rootState }, bidAmount) => {
    commit("updateBid", { bidAmount, rootState });
  },
  updateAsk: ({ commit, rootState }, askAmount) => {
    commit("updateAsk", { askAmount, rootState });
  },
  compareBestBid: ({ commit, rootState }) => {
    commit("compareBestBid", rootState);
  },
  compareBestAsk: ({ commit, rootState }) => {
    commit("compareBestAsk", rootState);
  },
  matchOrder: ({ commit, rootState }) => {
    commit("matchOrder", rootState);
  },
};
