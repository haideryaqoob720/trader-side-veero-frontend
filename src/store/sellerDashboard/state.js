export default {
  // GETTING PROFILE
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  password: null,
  // UPDATING PROFILE
  updateFirstName: null,
  updateLastName: null,
  updateEmail: null,
  updatePassword: null,
  // PURCHASINGS & SELLINGS
  buyings: [],
  sellings: [],
};
