export default {
  getProfile: ({ commit, rootState }) => {
    commit("getProfile", rootState);
  },
  updateProfile: ({ commit, rootState }) => {
    commit("updateProfile", rootState);
  },
  getBuyings: ({ commit, rootState }) => {
    commit("getBuyings", rootState);
  },
  getSellings: ({ commit, rootState }) => {
    commit("getSellings", rootState);
  },
};
