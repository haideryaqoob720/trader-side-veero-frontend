export default {
  getMvp: (state) => {
    console.log("working on mvp");
    axios
      .get("/headerimage/getimage")
      .then(({ data }) => {
        state.mvp = data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getLanding: (state, rootState) => {
    rootState.loading = true;
    const getBanners = axios.get("banners/showAll/1");
    const getPopularProducts = axios.get("/popularProducts/dashboard");
    const getMostPopular = axios.get("/mostPopularProduct/dashboard");
    Promise.all([getBanners, getPopularProducts, getMostPopular]).then(
      (res) => {
        rootState.loading = false;
        // banners
        state.banners = res[0].data.result[1];
        // popular products
        state.popularProducts = res[1].data;
        // most popular
        state.mostPopular = res[2].data;
      }
    );
  },
};
