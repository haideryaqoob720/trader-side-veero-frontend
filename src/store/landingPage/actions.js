export default {
  getMvp: ({ commit }) => {
    commit("getMvp");
  },
  getLanding: ({ commit, rootState }) => {
    commit("getLanding", rootState);
  },
};
